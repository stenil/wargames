import edu.ntnu.idatt2001.stenil.wargames.models.Army;
import edu.ntnu.idatt2001.stenil.wargames.models.ArmyFileWriter;
import edu.ntnu.idatt2001.stenil.wargames.models.InfantryUnit;
import edu.ntnu.idatt2001.stenil.wargames.models.RangedUnit;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class ArmyFileWriterTest {

    public Army makeTestArmy(){
        Army army = new Army("Test Army");

        for(int i = 0; i < 10; i++){
            army.addUnit(new InfantryUnit("Test Infantry", 100));
        }

        for(int i = 0; i < 10; i++){
            army.addUnit(new RangedUnit("Test Ranged", 100));
        }

        return army;
    }

    @Test
    void testWriteArmyFile() {

        ArmyFileWriter armyFileWriter = new ArmyFileWriter();

        Army army = makeTestArmy();

        File file = new File("src/test/resources/testArmy.csv");
        System.out.println(file);

        try {
            armyFileWriter.writeArmy(file,army);
        }catch (IOException e){
            fail();
        }
    }

    @Test
    void testWriteArmyFileWhenArmyIsNull() {
        Army army = null;
        ArmyFileWriter armyFileWriter = new ArmyFileWriter();
        File file = new File("src/test/resources/nullObject.csv");

        try {
            armyFileWriter.writeArmy(file, army);
        }catch (IOException e){
            assertEquals(e.getMessage(), "The Army object is null");
        }

        assertThrows(IOException.class, () -> {
           armyFileWriter.writeArmy(file, army);
        });
    }
}

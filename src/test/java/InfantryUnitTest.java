import edu.ntnu.idatt2001.stenil.wargames.models.InfantryUnit;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class InfantryUnitTest {

    @Test
    public void testOriginalConstructorForInfantryUnit () {
        InfantryUnit infantryUnit = new InfantryUnit("Footman", 100, 15, 10);

        assertNotNull(infantryUnit);
        assertEquals("Footman", infantryUnit.getName());
        assertEquals(100, infantryUnit.getHealth());
        assertEquals(15, infantryUnit.getAttack());
        assertEquals(10, infantryUnit.getArmor());
        assertEquals(2, infantryUnit.getAttackBonus());
        assertEquals(1, infantryUnit.getResistanceBonus());
    }
}

import edu.ntnu.idatt2001.stenil.wargames.models.RangedUnit;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RangedUnitTest {

    @Test
    public void testOriginalConstructorForRangedUnit () {
            RangedUnit rangedUnit = new RangedUnit("Archer", 100, 15, 8);

            assertNotNull(rangedUnit);
            assertEquals("Archer", rangedUnit.getName());
            assertEquals(100, rangedUnit.getHealth());
            assertEquals(15, rangedUnit.getAttack());
            assertEquals(8, rangedUnit.getArmor());
            assertEquals(3, rangedUnit.getAttackBonus());
            assertEquals(6, rangedUnit.getResistanceBonus());
    }
}

import edu.ntnu.idatt2001.stenil.wargames.models.CommanderUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CommanderUnitTest {

    @Test
    public void testOriginalConstructorForCommanderUnit() {
        CommanderUnit commanderUnit = new CommanderUnit("Mountain King", 180, 25, 15);

        assertNotNull(commanderUnit); //Checks if there is a value
        assertEquals("Mountain King", commanderUnit.getName());
        assertEquals(180, commanderUnit.getHealth());
        assertEquals(25, commanderUnit.getAttack());
        assertEquals(15, commanderUnit.getArmor());
        assertEquals(4, commanderUnit.getAttackBonus());
        assertEquals(1, commanderUnit.getResistanceBonus());
    }
}

import edu.ntnu.idatt2001.stenil.wargames.models.CavalryUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CavalryUnitTest {
    
    @Test
    public void testOriginalConstructorForCavalryUnit() {
        CavalryUnit cavalryUnit = new CavalryUnit("Knight", 100, 20, 12);

        assertNotNull(cavalryUnit);
        assertEquals("Knight", cavalryUnit.getName());
        assertEquals(100, cavalryUnit.getHealth());
        assertEquals(20, cavalryUnit.getAttack());
        assertEquals(12, cavalryUnit.getArmor());
        assertEquals(4,cavalryUnit.getAttackBonus());
        assertEquals(1, cavalryUnit.getResistanceBonus());
    }
}

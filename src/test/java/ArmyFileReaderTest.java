import edu.ntnu.idatt2001.stenil.wargames.models.Army;
import edu.ntnu.idatt2001.stenil.wargames.models.ArmyFileReader;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;


class ArmyFileReaderTest {

    @Test
    void testReadArmyFromFile() {
        ArmyFileReader armyFileReader = new ArmyFileReader();
        try {
            Army armies = armyFileReader.readArmy(new File
                    ("src/test/resources/testArmy.csv"));
            //Takes inn just one army
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    void testThatExceptionsAreThrownWhenArmyFileIsNotCsv() {
        Army army = null;
        ArmyFileReader armyFileReader = new ArmyFileReader();

        File file = new File("src/test/resources/notCSVFile");

        try {
            armyFileReader.readArmy(file);
            fail();
        } catch (IOException e) {
            assertEquals(e.getMessage(), "Unsupported file format. Only .csv-files are supported.");
        }

        assertThrows(IOException.class, () -> {
            armyFileReader.readArmy(file);
        });
    }

    @Test
    void testThatExceptionsAreThrownWhenArmyFileIsCorrupt(){
        Army army = null;
        File file = new File("src/test/resources/testArmyNegativeValues.csv");
        ArmyFileReader armyFileReader = new ArmyFileReader();

        try {
            armyFileReader.readArmy(file);
        } catch (IllegalArgumentException | IOException e) {
            assertEquals(e.getMessage(), "ERROR: Could not find any units");
        }

        assertThrows(IllegalArgumentException.class, () -> {
           armyFileReader.readArmy(file);
        });
    }
}
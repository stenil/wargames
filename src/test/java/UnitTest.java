import edu.ntnu.idatt2001.stenil.wargames.models.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class UnitTest {

    @Test
    public void creationOfUnit() {
        RangedUnit u1 = new RangedUnit("Archer", 100, 15, 8);
        InfantryUnit u2 = new InfantryUnit("Footman", 100, 15, 10);
        CavalryUnit u3 = new CavalryUnit("Knight", 100, 20, 12);
        CommanderUnit u4 = new CommanderUnit("Mountain King", 180, 25, 15);

        //Checks if they have a value
        assertNotNull(u1);
        assertNotNull(u2);
        assertNotNull(u3);
        assertNotNull(u4);
    }

    // TODO: 23/05/2022 dele opp denne metoden  
    
    @Test
    public void testIfAttackWorks() {
        RangedUnit archer = new RangedUnit("Archer", 100, 15, 8);
        InfantryUnit footman = new InfantryUnit("Footman", 100, 15, 10);
        CavalryUnit knight = new CavalryUnit("Knight", 100, 20, 12);
        CommanderUnit mountain_king = new CommanderUnit("Mountain King", 180, 25, 15);

        //Test 1
        assertEquals(100, footman.getHealth());
        archer.attack(footman, Terrain.FORESTS);
        assertEquals(94, footman.getHealth()); //Dmg = 6
        archer.attack(footman, Terrain.FORESTS);
        assertEquals(88, footman.getHealth()); //Dmg = 6

        //Archer attack on static defence value of infantry unit is consistent at a value of 6.

        //Test 2
        assertEquals(100, archer.getHealth());
        footman.attack(archer, Terrain.PLAINS);
        assertEquals(97, archer.getHealth()); //Dmg = 3
        footman.attack(archer, Terrain.FORESTS);
        assertEquals(92, archer.getHealth()); //Dmg = 5
        footman.attack(archer, Terrain.FORESTS);
        assertEquals(85, archer.getHealth()); //Dmg = 7
        footman.attack(archer, Terrain.PLAINS);
        assertEquals(78, archer.getHealth()); //Dmg = 7

        //Archer resistance is decreasing with every attack,
        //levelling at a value of 7, implying that the resistanceBonus method works.


        //Test 3
        assertEquals(180, mountain_king.getHealth());
        knight.attack(mountain_king, Terrain.PLAINS);
        assertEquals(170, mountain_king.getHealth()); //Dmg = 10
        knight.attack(mountain_king, Terrain.FORESTS);
        assertEquals(163, mountain_king.getHealth()); //Dmg = 7
        knight.attack(mountain_king, Terrain.HILLS);
        assertEquals(157, mountain_king.getHealth()); //Dmg = 6

        //Knight attack is decreasing after the first attack,
        //levelling at a value of 6, implying that the attackBonus method works.

        //Test 4
        assertEquals(100, knight.getHealth());
        mountain_king.attack(knight, Terrain.FORESTS);
        assertEquals(83, knight.getHealth()); //Dmg = 17
        mountain_king.attack(knight, Terrain.FORESTS);
        assertEquals(68, knight.getHealth()); //Dmg = 15
        mountain_king.attack(knight, Terrain.PLAINS);
        assertEquals(52, knight.getHealth()); //Dmg = 15

        //Mountain King attack is decreasing after the first attack,
        //levelling at a value of 15, implying that the attackBonus method works.
    }
}

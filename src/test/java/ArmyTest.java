import edu.ntnu.idatt2001.stenil.wargames.models.Army;
import edu.ntnu.idatt2001.stenil.wargames.models.RangedUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ArmyTest {
    
    @Test
    public void testAddingUnits() {
        Army testArmy = new Army("Human Army");
        RangedUnit testArcher = new RangedUnit("Archer", 100, 15, 8);
        testArmy.addUnit(testArcher);

        assertNotNull(testArcher);
        assertNotNull(testArmy);
        assertEquals(1, testArmy.getUnit().size());
    }

    @Test
    public void testRemovingUnits() {
        Army testArmy = new Army("Human Army");
        RangedUnit testArcher = new RangedUnit("Archer", 100, 15, 8);

        assertNotNull(testArcher);
        assertEquals(0, testArmy.getUnit().size());
        testArmy.addUnit(testArcher);
        assertEquals(1, testArmy.getUnit().size());
        testArmy.removeUnit(testArcher);
        assertEquals(0, testArmy.getUnit().size());
    }
}


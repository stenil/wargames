import edu.ntnu.idatt2001.stenil.wargames.models.Army;
import edu.ntnu.idatt2001.stenil.wargames.models.Battle;
import edu.ntnu.idatt2001.stenil.wargames.models.RangedUnit;
import edu.ntnu.idatt2001.stenil.wargames.models.Terrain;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BattleTest {
    
    @Test
    public void testOriginalConstructorForCreatingAnArmy() {
        Army armyOne = new Army("Human Army");
        Army armyTwo = new Army("Orc Army");

        RangedUnit rangedUnit = new RangedUnit("Archer", 100, 15, 8);
        RangedUnit spearman = new RangedUnit("Spearman", 100, 15, 8);
        armyOne.addUnit(rangedUnit);
        armyTwo.addUnit(spearman);

        Battle battle = new Battle(armyOne, armyTwo, Terrain.FORESTS);
        assertEquals(1, armyOne.getUnit().size());
        assertEquals(1, armyTwo.getUnit().size());
        assertNotNull(battle);
    }
}

package edu.ntnu.idatt2001.stenil.wargames.models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

/**
 * An ArmyFileWriter makes it possible to write Army objects to a file.
 * Each army file consists of four unique types of army units on the format 'type','name','health'.
 */

public class ArmyFileWriter {

    private static final String NEWLINE = "\n";
    private static final String DELIMITER = ",";

    public void writeArmy(File file, Army army) throws IOException {
        if (!file.getName().endsWith(".csv")) {
            throw new IOException("Unsupported file format. Only .csv-files are supported.");
        }
        if(Objects.isNull(army)){
            throw new IOException("The Army object is null");
        }
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(army.getNameOfArmy()+NEWLINE); {
                for (Unit unit: army.getAllUnits()) {
                    String unitTypeSubString = unit.getClass().toString();
                    String line = unitTypeSubString.substring(unitTypeSubString.lastIndexOf(".") + 1) + DELIMITER
                            + unit.getName() + DELIMITER
                            + unit.getHealth();
                    fileWriter.write(line + NEWLINE);
                }
            }
        } catch (IOException e) {
            throw new IOException("Unable to write army file: " + e.getMessage());
        }
    }
}

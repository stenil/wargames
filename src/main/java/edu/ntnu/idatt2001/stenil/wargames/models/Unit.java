package edu.ntnu.idatt2001.stenil.wargames.models;

/**
 * Class for holding a Unit in an Army
 */
public abstract class Unit {

    private String name;
    private int health;
    private int attack;
    private int armor;

    /**
     * Constructor which creates an instance of a unit.
     * @param name the unit name
     * @param health the unit's health
     * @param attack the unit's attack value
     * @param armor the unit's defence rating
     * @throws IllegalArgumentException an exception is thrown if unit criteria is not met or
     * breaches with logic.
     */
    public Unit (String name, int health, int attack, int armor)
            throws IllegalArgumentException {

        if(!(health >= 0)) {
            throw new IllegalArgumentException("Health cannot be below 0.");}
        if (!(armor >= 0)) {
            throw new IllegalArgumentException("Armor cannot be below 0.");}
        if (!(attack >= 0)) {
            throw new IllegalArgumentException("Attack cannot be below 0.");}
        if (name.isEmpty()) {
            throw new IllegalArgumentException("The name of the unit cannot be empty.");}

        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * A method applicable to determine whether/when a unit is alive or dead.
     * @return a boolean expression, true implies a dead unit, false implies a live unit.
     */
    public boolean isDead() {
        return health <= 0; //If 0 then True, you dead.
    }

    /**
     * Method for attacking an opponent
     * @param opponent is the unit being inflicted damage upon (by another unit)
     */
    public void attack(Unit opponent, Terrain terrain) {

        int opponentHealth = opponent.getHealth();
        int damage = this.attack + this.getAttackBonus() + this.getTerrainBonusAttack(terrain);
        int armor = opponent.getArmor() + opponent.getResistanceBonus() + this.getTerrainBonusDefence(terrain);

        if (opponentHealth - damage + armor <= 0){
            opponent.setHealth(0);
        }
        else if(armor < damage){
            opponent.setHealth(opponentHealth - damage + armor);
        }
    }

    /**
     * Method to retrieve the name of a unit
     * @return the unit name as a String
     */
    public String getName() {
        return name;
    }

    /**
     * Method for setting the name of a unit
     * @param name the unit name as a String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to retrieve the health value of a unit
     * @return the unit health value as an integer
     */
    public int getHealth() {
        return health;
    }

    /**
     * Method to retrieve the attack value of a unit
     * @return the unit attack value as an integer
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Method to retrieve the armor rating/value of a unit
     * @return the unit armor rating/value as an integer
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Method to set the health value of a unit
     * @param health is the unit's health value as an integer
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Method to set the armor rating/value of a unit
     * @param armor is the unit's armor rating/value as an integer
     */
    public void setArmor(int armor) { // Will most likely never be used
        this.armor = armor;
    }

    /**
     * An abstract method to retrieve the unit attack bonus,
     * which is an offensive value added to the unit's base attack value.
     * The attack bonus is unique for each type of unit.
     * @return abstract
     */
    public abstract int getAttackBonus();

    /**
     * An abstract method to retrieve the terrain bonus attack damage for a unit,
     * which is a unit-specific additional offensive value to the unit's existing attack bonus value.
     * @param terrain is the selected terrain, either Forests, Hills, or Plains.
     * @return abstract
     */
    public abstract int getTerrainBonusAttack(Terrain terrain);

    /**
     * An abstract method to retrieve the terrain bonus defence rating for a unit,
     * which is a unit-specific additional defencive value to the unit's existing resistance bonus value.
     * @param terrain is the selected terrain, either Forests, Hills, or Plains.
     * @return abstract
     */
    public abstract int getTerrainBonusDefence(Terrain terrain);

    /**
     * An abstract method to retrieve the unit resistance bonus,
     * which is a defencive value added to the unit's armor rating/value.
     * The resistance bonus is unique for each type of unit.
     * @return abstract.
     */
    public abstract int getResistanceBonus();

    /**
     * Method to print a unit's stats in an understandable manner.
     * @return Unit name, attack value, health, and armor rating/value
     */
    public String getUnitAsString() {
        return String.format("Name: %s | Attack: %s | Health: %s | Armor: %s", name, attack, health, armor);
    }

    @Override
    public String toString() {
        return "Unit name: " + name + " | " +
                "Attack points: " + attack + " | " +
                "Health: " + health + " | " +
                "Armour points: " + armor + " | " + "\n";
    }
}
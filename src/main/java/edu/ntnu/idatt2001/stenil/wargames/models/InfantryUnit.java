package edu.ntnu.idatt2001.stenil.wargames.models;

/**
 * Class for holding an Infantry unit.
 */
public class InfantryUnit extends Unit {

    /**
     * Constructor for an infantry unit, which extends the superclass edu.ntnu.idatt2001.stenil.wargames.models.Unit
     * @param name is the name of an infantry unit as a String
     * @param health is the health value of an infantry unit as an int
     * @param attack is the attack value of an infantry unit as an int
     * @param armor is the armor value of an infantry unit as an int
     */
    public InfantryUnit (String name,
                         int health,
                         int attack,
                         int armor) {
        super (name, health, attack, armor);
    }

    /**
     * Simplified constructor for an infantry unit,
     * creating a new and specified instance with given attack and armor values
     * @param name is the name of an infantry unit as a String
     * @param health is the health value of an infantry unit as an int
     */
    public InfantryUnit (String name, int health) throws IllegalArgumentException {
        super (name, health, 15, 10);
    }

    /**
     * Method which provides the infantry unit with an additional attack/offensive value
     * @return attack bonus value
     */
    @Override
    public int getAttackBonus(){
        return 2;
    }

    /**
     * Method which provides the infantry unit with an additional armor/defensive value
     * @return resistance bonus value
     */
    @Override
    public int getResistanceBonus() {
        return 1;
    }

    /**
     * Method which provides the infantry unit with an additional attack value,
     * if the chosen terrain is forests.
     * @param terrain
     * @return the terrain effect bonus to the unit's base attack value
     */
    @Override
    public int getTerrainBonusAttack(Terrain terrain) {
        if (terrain.equals(Terrain.FORESTS)) {
            return 2;
            //Additional attack bonus when fighting in Forests
        }
        return 0;
    }

    /**
     * Method which provides the infantry unit with an additional defensive value,
     * if the chosen terrain is forests.
     * @param terrain
     * @return the terrain effect bonus to the unit's base defense value
     */
    @Override
    public int getTerrainBonusDefence(Terrain terrain) {
        if (terrain.equals(Terrain.FORESTS)) {
            return 2;
        }
        return 0;
    }
}


package edu.ntnu.idatt2001.stenil.wargames.models;

/**
 * Class for holding a Commander unit.
 */
public class CommanderUnit extends CavalryUnit {

    private int attackBonus = 6;

    /**
     * Constructor for a commander unit, which extends the superclass edu.ntnu.idatt2001.stenil.wargames.models.Unit
     * @param name is the name of a commander unit as a String
     * @param health is the health value of a commander unit as an int
     * @param attack is the attack value of a commander unit as an int
     * @param armor is the armor value of a commander unit as an int
     */
    public CommanderUnit (String name,
                        int health,
                        int attack,
                        int armor) {
        super (name, health, attack, armor);
    }

    /**
     * Simplified constructor for a commander unit
     * creating a new and specified instance with given attack and armor values
     * @param name is the name of a commander unit as a String
     * @param health is the health value of a commander unit as an int
     */
    public CommanderUnit (String name,int health) throws IllegalArgumentException {
        super (name, health, 25, 15);
    }

    /**
     * Method which provides the commander unit with an additional attack/offensive value
     * @return attack bonus value
     */
    @Override
    public int getAttackBonus() {
        if (attackBonus == 2) {
            return attackBonus;
        }
        attackBonus -= 2;
        return attackBonus;
    }

    /**
     * Method which provides the commander unit with an additional armor/defensive value
     * @return resistance bonus value
     */
    @Override
    public int getResistanceBonus() {
        return 1;
    }

}

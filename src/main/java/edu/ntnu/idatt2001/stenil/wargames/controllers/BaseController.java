package edu.ntnu.idatt2001.stenil.wargames.controllers;

import edu.ntnu.idatt2001.stenil.wargames.View;

/**
 * Considering that every controller requires an instance of view, it is beneficial to include a base controller
 */
public class BaseController {
    static View view = View.getInstance();
}
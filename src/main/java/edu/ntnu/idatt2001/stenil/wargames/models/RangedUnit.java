package edu.ntnu.idatt2001.stenil.wargames.models;

/**
 * Class for holding a Ranged unit.
 */
public class RangedUnit extends Unit {

    private int resistBonus = 8;

    /**
     * Constructor for a ranged unit, which extends the superclass edu.ntnu.idatt2001.stenil.wargames.models.Unit
     * @param name  is the name of the ranged unit as a String
     * @param health is the health value of the ranged unit as an int
     * @param attack is the attack value of the ranged unit as an int
     * @param armor is the armor value of the ranged unit as an int
     */
    public RangedUnit (String name,
                       int health,
                       int attack,
                       int armor) {
        super (name, health, attack, armor);
    }

    /**
     * Simplified constructor for a ranged unit,
     * creating a new and specified instance with given attack and armor values
     * @param name is the name of the ranged unit as a String
     * @param health is the health value of the range unit as an int
     */
    public RangedUnit(String name, int health) throws IllegalArgumentException {
        super (name, health, 15, 8);
    }

    /**
     * A method to define the specific attack bonus for a ranged unit
     * Considering that this unit can attack from distance,
     * a considerable bonus is given.
     * @return the bonus value to the unit's base attack value
     */
    @Override
    public int getAttackBonus(){
       return 3;
    }

    /**
     * Method which provides the ranged unit with an additional armor/defensive value
     * @return resistance bonus value
     */
    @Override
    public int getResistanceBonus() {
        if (resistBonus == 2 ){
            return resistBonus;
        }
        resistBonus -= 2;
        return resistBonus;
    }

    /**
     * Method which provides the ranged unit with an additional attack value,
     * if the chosen terrain is forests.
     * @param terrain
     * @return the terrain effect bonus to the unit's base attack value
     */
    @Override
    public int getTerrainBonusAttack (Terrain terrain) {
        if (terrain.equals(Terrain.HILLS)) {
            return 1;
            //Additional attack bonus when fighting in Hills
        } else if (terrain.equals(Terrain.FORESTS)) {
            return -1;
            //Reduced attack bonus when fighting in Forests.
        }
        return 0;
    }

    /**
     * Method which provides the ranged unit with an additional defensive value,
     * if the chosen terrain is forests.
     * @param terrain
     * @return the terrain effect bonus to the unit's base defense value
     */
    @Override
    public int getTerrainBonusDefence(Terrain terrain) {
        return 0; //Unit Defence Bonus not affected by terrain
    }

}

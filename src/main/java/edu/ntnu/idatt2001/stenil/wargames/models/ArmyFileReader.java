package edu.ntnu.idatt2001.stenil.wargames.models;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * An ArmyFileReader makes it possible to write Army objects to a file.
 */
public class ArmyFileReader {

    private static final String DELIMITER = ",";

    public ArmyFileReader() {}

    public Army readArmy(File file) throws IOException {
        if (!file.getName().endsWith(".csv")) {
            throw new IOException("Unsupported file format. Only .csv-files are supported.");
        }

        Army army = null;
        String armyName = null;
        List<Unit> units = new ArrayList<>();

        try (Scanner scanner = new Scanner(file)) {

            if (scanner.hasNext()) {
                armyName = scanner.nextLine();
            }

            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                String[] tokens = line.split(DELIMITER);
                if (tokens.length != 3) {
                    throw new IOException("Line data '" + line + "' is invalid. " +
                            "Make sure each line is on the form 'unit type, unit name, unit health.'");
                }

                String unitType = tokens[0];
                String unitName = tokens[1];
                int health;
                try {
                    health = Integer.parseInt(tokens[2]);
                } catch (IllegalArgumentException e) {
                    throw new IOException("Unit health must be an integer (" + e.getMessage() + ")");
                }

                Unit unit = createObjects(unitType);
                unit.setName(unitName);
                unit.setHealth(health);

                units.add(unit);
            }
        } catch (IOException e) {
            throw new IOException("Unable to read army file: " + e.getMessage());
        }
        return new Army(armyName, units);
    }

    /**
     * Method for creating (dummy) unit objects
     * @param klasse is the corresponding class of a unit
     * @return the units in question
     */
    public Unit createObjects (String klasse) {
        if (klasse.equalsIgnoreCase("InfantryUnit")) {
            return new InfantryUnit("unitName", 100);
            //Using simplified constructor for an infantry unit
        }
        if (klasse.equalsIgnoreCase("RangedUnit")) {
            return new RangedUnit("unitName", 100);
            //Using simplified constructor for a ranged unit
        }
        if (klasse.equalsIgnoreCase("CavalryUnit")) {
            return new CavalryUnit("unitName", 100);
            //Using simplified constructor for a cavalry unit
        }
        if (klasse.equalsIgnoreCase("CommanderUnit")) {
            return new CommanderUnit("unitName", 180);
            //Using simplified constructor for a commander unit
        }
        throw new IllegalArgumentException("ERROR: Could not find any units");
    }
}


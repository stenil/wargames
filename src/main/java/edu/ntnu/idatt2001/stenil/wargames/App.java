package edu.ntnu.idatt2001.stenil.wargames;

import javafx.application.Application;
import javafx.stage.Stage;

public class App extends Application {
    private final static String title = "Wargames";

    @Override
    public void start(Stage primaryStage) throws Exception {
        View view = View.getInstance();
        view.setStage(primaryStage);
        view.setCurrentScene(View.OPENING_SCENE);
        primaryStage.show();
    }

    public static void main(String[] args) {
            launch(args);
    }

    private void setTitle(Stage stage) {
        stage.setTitle(title);
    }
}
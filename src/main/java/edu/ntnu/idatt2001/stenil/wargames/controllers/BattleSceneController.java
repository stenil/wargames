package edu.ntnu.idatt2001.stenil.wargames.controllers;

import edu.ntnu.idatt2001.stenil.wargames.View;
import edu.ntnu.idatt2001.stenil.wargames.models.*;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static edu.ntnu.idatt2001.stenil.wargames.controllers.BaseController.view;

/**
 * Controller for battle-scene.
 */
public class BattleSceneController {

    private Army army1;
    private Army army2;

    @FXML
    public Text battleResultStatus1Green, battleResultStatus1Red,
            battleResultStatus2Green, battleResultStatus2Red;

    @FXML
    public CheckBox checkboxPlains, checkboxHills, checkboxForests;

    @FXML
    public TextField Army1Name, Army1TotalAmountOfUnits,
            Army1InfantryUnitAmount, Army1InfantryUnitName,
            Army1RangedUnitAmount, Army1RangedUnitName,
            Army1CavalryUnitAmount, Army1CavalryUnitName,
            Army1CommanderUnitAmount, Army1CommanderUnitName,
            Army1InfantryUnitAttDefHp, Army1RangedUnitAttDefHp, Army1CavalryUnitAttDefHp, Army1CommanderUnitAttDefHp;

    @FXML
    public TextField Army2Name, Army2TotalAmountOfUnits,
            Army2InfantryUnitAmount, Army2InfantryUnitName,
            Army2RangedUnitAmount, Army2RangedUnitName,
            Army2CavalryUnitAmount, Army2CavalryUnitName,
            Army2CommanderUnitAmount, Army2CommanderUnitName,
            Army2InfantryUnitAttDefHp, Army2RangedUnitAttDefHp, Army2CavalryUnitAttDefHp, Army2CommanderUnitAttDefHp;

    @FXML
    public Button loadArmyPlayer1, loadArmyPlayer2, quickBattle;

    @FXML
    public ProgressIndicator battleStatusIndicatorPlayer1, battleStatusIndicatorPlayer2;

    /**
     * Method which is called when the Load Army Player 1 or Load Army Player 2 buttons are pressed.
     * @return an Army from a file.
     */
    @FXML
    public Army loadArmyFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(
                "src/main/resources/edu/ntnu/idatt2001/stenil/wargames/premade-armies"));
        //Directory path from content root
        fileChooser.setTitle("Choose Army");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Armies","*.csv"));
        File selectedFile = fileChooser.showOpenDialog(view.getStage());
        try {
            if (selectedFile != null) {
                ArmyFileReader reader = new ArmyFileReader();
                return reader.readArmy(selectedFile);
            }
        } catch (IOException e) {
            e.printStackTrace(); //TODO: Make error message in GUI
        }
        return null;
    }

    /**
     * Method that gets called when the Load Army Player 1 button is pressed
     */
    @FXML
    public void loadArmyPlayer1() {
        Army army1 = loadArmyFile();
        this.army1 = army1;

        battleDetailsArmy1(army1);
        battleStatusArmy1(army1);
    }

    /**
     * Method that gets called when the Load Army Player 2 button is pressed
     */
    @FXML
    public void loadArmyPlayer2() {
        Army army2 = loadArmyFile();
        this.army2 = army2;

        battleDetailsArmy2(army2);
        battleStatusArmy2(army2);
    }

    /**
     * Method that gets called with the Start button is pressed
     */
    public void startBattle() {
        Army copyArmy1 = getCopyOfArmy(army1);
        Army copyArmy2 = getCopyOfArmy(army2);
        Battle battle = new Battle(copyArmy1,copyArmy2, chooseTerrain());
        CheckBox plains = checkboxPlains;
        CheckBox hills = checkboxHills;
        CheckBox forests = checkboxForests;

        if (!plains.isSelected() && !hills.isSelected() && !forests.isSelected()) {
            PopupMessage.notificationErrorMessage("Error", "Battle simulation",
                    "Could not find selected terrain. Please choose a terrain by ticking one of the " +
                            "indicated boxes on the screen.");

        } else if ((plains.isSelected() && hills.isSelected() && forests.isSelected()) ||
                (plains.isSelected() && hills.isSelected() && !forests.isSelected()) ||
                (plains.isSelected() && !hills.isSelected() && forests.isSelected()) ||
                (!plains.isSelected() && hills.isSelected() && forests.isSelected())) {
            PopupMessage.notificationErrorMessage("Error", "Battle simulation",
                    "Too many terrains have been selected. Please select only one terrain from the " +
                            "'Select terrain' battle modifier");

        } else if (checkboxPlains.isSelected() || checkboxHills.isSelected() || checkboxForests.isSelected()) {
            battle.simulateBattle();
        }

        if (copyArmy1.hasUnits()) {
            battleResultStatus1Green.setText("Victorious!");
            battleStatusIndicatorPlayer1.setProgress(1); //Icon indicating victory
            battleResultStatus2Red.setText("Defeat");
            battleStatusIndicatorPlayer2.setProgress(0); //Icon indicating defeat
        } else {
            battleResultStatus2Green.setText("Victorious!");
            battleStatusIndicatorPlayer2.setProgress(1); //Icon indicating victory
            battleResultStatus1Red.setText("Defeat");
            battleStatusIndicatorPlayer1.setProgress(0); //Icon indicating defeat
        }
        battleStatusArmy1(copyArmy1);
        battleStatusArmy2(copyArmy2);
    }

    public Terrain chooseTerrain () {
        if (checkboxForests.isSelected()) {
            return Terrain.FORESTS;
        }
        if (checkboxHills.isSelected()) {
            return Terrain.HILLS;
        }
        if (checkboxPlains.isSelected()) {
            return Terrain.PLAINS;
        }
        return null;
    }

    /**
     * Method for retrieving a gopy of an Army.
     * @param army army parameter
     * @return a copy of the selected Army.
     */
    public Army getCopyOfArmy(Army army) {

        Army copyArmy = new Army(army.getNameOfArmy());
        for (Unit unit : army.getInfantryUnits()) {
            copyArmy.addUnit(new InfantryUnit(unit.getName(), unit.getHealth(), unit.getAttack(), unit.getArmor()));
        }
        for (Unit unit : army.getRangedUnits()) {
            copyArmy.addUnit(new RangedUnit(unit.getName(), unit.getHealth(), unit.getAttack(), unit.getArmor()));
        }
        for (Unit unit : army.getCavalryUnits()) {
            copyArmy.addUnit(new CavalryUnit(unit.getName(), unit.getHealth(), unit.getAttack(), unit.getArmor()));
        }
        for (Unit unit : army.getCommanderUnits()) {
            copyArmy.addUnit(new CommanderUnit(unit.getName(), unit.getHealth(), unit.getAttack(), unit.getArmor()));
        }
        return copyArmy;
    }

    /**
     * Method for resetting the armies.
     */
    @FXML
    public void resetArmies() {
        battleResultStatus1Red.setText("");
        battleResultStatus1Green.setText("");
        battleResultStatus2Red.setText("");
        battleResultStatus2Green.setText("");
        battleStatusIndicatorPlayer1.setProgress(0);
        battleStatusIndicatorPlayer2.setProgress(0);

        battleDetailsArmy1(army1);
        battleDetailsArmy2(army2);

        battleStatusArmy1(army1);
        battleStatusArmy2(army2);

    }

    /**
     * Method for setting the details about an Army's units.
     * @param army1 parameter.
     */
    public void battleDetailsArmy1(Army army1) {
        Army1Name.setText(army1.getNameOfArmy());
        Army1InfantryUnitName.setText(army1.getInfantryUnits().get(0).getName());
        Army1RangedUnitName.setText(army1.getRangedUnits().get(0).getName());
        Army1CavalryUnitName.setText(army1.getCavalryUnits().get(0).getName());
        Army1CommanderUnitName.setText(army1.getCommanderUnits().get(0).getName());

        Army1InfantryUnitAttDefHp.setText("15/10/" + army1.getInfantryUnits().get(0).getHealth());
        Army1RangedUnitAttDefHp.setText("15/8/" + army1.getInfantryUnits().get(0).getHealth());
        Army1CavalryUnitAttDefHp.setText("20/12/" + army1.getCavalryUnits().get(0).getHealth());
        Army1CommanderUnitAttDefHp.setText("25/15/" + army1.getCommanderUnits().get(0).getHealth());
    }

    /**
     * Method fpr setting the details about an Army's units.
     * @param army2 parameter.
     */
    public void battleDetailsArmy2(Army army2) {
        Army2Name.setText(army2.getNameOfArmy());
        Army2InfantryUnitName.setText(army2.getInfantryUnits().get(0).getName());
        Army2RangedUnitName.setText(army2.getRangedUnits().get(0).getName());
        Army2CavalryUnitName.setText(army2.getCavalryUnits().get(0).getName());
        Army2CommanderUnitName.setText(army2.getCommanderUnits().get(0).getName());

        Army2InfantryUnitAttDefHp.setText("15/10/" + army2.getInfantryUnits().get(0).getHealth());
        Army2RangedUnitAttDefHp.setText("15/8/" + army2.getInfantryUnits().get(0).getHealth());
        Army2CavalryUnitAttDefHp.setText("20/12/" + army2.getCavalryUnits().get(0).getHealth());
        Army2CommanderUnitAttDefHp.setText("25/15/" + army2.getCommanderUnits().get(0).getHealth());
    }

    /**
     * Method for setting the total number of units from an Army.
     * @param army1 parameter.
     */
    public void battleStatusArmy1(Army army1) {
        Army1TotalAmountOfUnits.setText(Integer.toString(army1.getAllUnits().size()));
        Army1InfantryUnitAmount.setText(Integer.toString(army1.getInfantryUnits().size()));
        Army1RangedUnitAmount.setText(Integer.toString(army1.getRangedUnits().size()));
        Army1CavalryUnitAmount.setText(Integer.toString(army1.getCavalryUnits().size()));
        Army1CommanderUnitAmount.setText(Integer.toString(army1.getCommanderUnits().size()));
    }

    /**
     * Method for settign the total number of units from an Army.
     * @param army2 parameter.
     */
    public void battleStatusArmy2(Army army2) {
        Army2TotalAmountOfUnits.setText(Integer.toString(army2.getAllUnits().size()));
        Army2InfantryUnitAmount.setText(Integer.toString(army2.getInfantryUnits().size()));
        Army2RangedUnitAmount.setText(Integer.toString(army2.getRangedUnits().size()));
        Army2CavalryUnitAmount.setText(Integer.toString(army2.getCavalryUnits().size()));
        Army2CommanderUnitAmount.setText(Integer.toString(army2.getCommanderUnits().size()));
    }

    /**
     * Method that is called when the Quick battle button is pressed.
     */
    @FXML
    public void loadQuickBattleDefaultArmies() {
        Army humanArmy = defaultArmies().get(0); //Defaults
        Army orcArmy = defaultArmies().get(1); //Defaults
        army1 = humanArmy;
        army2 = orcArmy;

        Army1Name.setText(humanArmy.getNameOfArmy());
        Army2Name.setText(orcArmy.getNameOfArmy());

        battleDetailsArmy1(army1);
        battleDetailsArmy2(army2);
        battleStatusArmy1(army1);
        battleStatusArmy2(army2);
    }

    /**
     * Method that is called when the Quick battle button is pressed.
     * @return a default Army as a list.
     */
    public List<Army> defaultArmies() {
        Army humanArmy = new Army("Human Army");
        Army orcArmy = new Army ("Orc Army");
        for (int i = 0; i < 50; i++) {
            humanArmy.addUnit(new InfantryUnit("Swordman " + i, 100));
            orcArmy.addUnit(new CavalryUnit("Raider " + i, 100));
        }
        for (int i = 0; i < 20; i++) {
            humanArmy.addUnit(new RangedUnit("Archer " + i, 100));
            humanArmy.addUnit(new CavalryUnit("Knight " + i, 100));
            orcArmy.addUnit(new InfantryUnit("Grunt " + i, 100));
            orcArmy.addUnit(new RangedUnit("Spearman " + i, 100));
        }
        for (int i = 0; i < 5; i++) {
            orcArmy.addUnit(new CommanderUnit("Nazgul " + i, 180));
            humanArmy.addUnit(new CommanderUnit("Mountain King " + i, 180));
        }
        return Arrays.asList(humanArmy, orcArmy);
    }

    /**
     * Method which redirects to the home screen when the button is pressed.
     */
    public void returnToHomeScreen() {
        view.setCurrentScene(View.OPENING_MENU);
    }
}

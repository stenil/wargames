package edu.ntnu.idatt2001.stenil.wargames.controllers;

import javafx.scene.control.Alert;

/**
 * Controller for popups.
 */
public class PopupMessage extends BaseController{

    /**
     * Method used to display an information pop-up alert or notification.
     * @param title the title of the notification as a String
     * @param header the header of the notification as a String
     * @param content the content of the notification as a String
     */
    public static void notificationErrorMessage(String title, String header, String content) {
        Alert a = new Alert(Alert.AlertType.ERROR);
        a.setTitle(title);
        a.setHeaderText(header);
        a.setContentText(content);
        a.show();
    }
}

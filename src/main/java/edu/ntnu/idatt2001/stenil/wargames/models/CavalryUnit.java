package edu.ntnu.idatt2001.stenil.wargames.models;

/**
 * Class that holds a Cavalry unit.
 */
public class CavalryUnit extends Unit {

    private int attackBonus = 6;

    /**
     * Constructor for a cavalry unit, which extends the superclass edu.ntnu.idatt2001.stenil.wargames.models.Unit
     * @param name  is the name of the cavalry unit as a String
     * @param health is the health value of the cavalry unit as an int
     * @param attack is the attack value of the cavalry unit as an int
     * @param armor is the armor value of the cavalry unit as an int
     */
    public CavalryUnit (String name,
                         int health,
                         int attack,
                         int armor) {
        super (name, health, attack, armor);
    }

    /**
     * Simplified constructor for a cavalry unit,
     * creating a new and specified instance with given attack and armor values
     * @param name is the name of a cavalry unit as a String
     * @param health is the health value of a cavalry unit as an int
     */
    public CavalryUnit (String name, int health) throws IllegalArgumentException {
        super (name, health, 20, 12);
    }

    /**
     * Method which provides the cavalry unit with an additional attack/offensive value
     * @return attack bonus value
     */
    @Override
    public int getAttackBonus() {
        if (attackBonus == 2) {
            return attackBonus;
        }
        attackBonus -= 2;
        return attackBonus;
    }
    /**
     * Method which provides the cavalry unit with an additional armor/defensive value
     * @return resistance bonus value
     */
    @Override
    public int getResistanceBonus() {
        return 1;
    }

    /**
     * Method which provides the cavalry unit with an additional attack value,
     * if the chosen terrain is plains.
     * @param terrain
     * @return the terrain effect bonus to the unit's base attack value
     */
    @Override
    public int getTerrainBonusAttack (Terrain terrain) {
        if (terrain.equals(Terrain.PLAINS)) {
            return 2;
            //Additional attack bonus when fighting in Hills
        }
        return 0;
    }

    /**
     * Method which provides the ranged unit with an additional defensive value,
     * if the chosen terrain is forests.
     * @param terrain
     * @return the terrain effect bonus to the unit's base defense value
     */
    @Override
    public int getTerrainBonusDefence(Terrain terrain) {
        if (terrain.equals(Terrain.FORESTS)) {
            return -1;
            //All defence bonus is set to 0 when terrain is forests.
        }
        return 0; //Unit Defence Bonus not affected by terrain
    }

}

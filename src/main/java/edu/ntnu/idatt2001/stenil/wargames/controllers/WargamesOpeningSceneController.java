package edu.ntnu.idatt2001.stenil.wargames.controllers;

import edu.ntnu.idatt2001.stenil.wargames.View;

/**
 * Controller for wargames-opening-scene.
 */
public class WargamesOpeningSceneController extends BaseController{

    public void navigateToMenu() {
        view.setCurrentScene(View.OPENING_MENU);
    }

    public void exitGame() {
        System.out.println("Thank you for playing Wargames!");
        System.exit(0);
    }
}
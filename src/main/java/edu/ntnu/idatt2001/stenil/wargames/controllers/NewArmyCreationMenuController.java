package edu.ntnu.idatt2001.stenil.wargames.controllers;

import edu.ntnu.idatt2001.stenil.wargames.View;
import edu.ntnu.idatt2001.stenil.wargames.factories.UnitFactory;
import edu.ntnu.idatt2001.stenil.wargames.models.Army;
import edu.ntnu.idatt2001.stenil.wargames.models.ArmyFileWriter;
import edu.ntnu.idatt2001.stenil.wargames.models.Unit;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static edu.ntnu.idatt2001.stenil.wargames.controllers.BaseController.view;

/**
 * Controller for new-army-creation-menu.
 */
public class NewArmyCreationMenuController {

    @FXML
    public Text armySaveFileMessage;

    @FXML
    public TextField armyName,
            InfantryUnitName, RangedUnitName, CavalryUnitName, CommanderUnitName,
            InfantryUnitHealth, RangedUnitHealth, CavalryUnitHealth, CommanderUnitHealth,
            InfantryUnitAmount, RangedUnitAmount, CavalryUnitAmount, CommanderUnitAmount;

    @FXML
    public Button InfantryAttack, RangedAttack, CavalryAttack, CommanderAttack,
            InfantryArmor, RangedArmor, CavalryArmor, CommanderArmor;

    @FXML
    public CheckBox checkboxInfantryUnit, checkboxRangedUnit, checkboxCavalryUnit, checkboxCommanderUnit;

    @FXML
    public void returnToMainMenu() {
        view.setCurrentScene(View.OPENING_MENU);
    }

    @FXML
    public void navigateToBattleScene() {
        view.setCurrentScene(View.BATTLE_SCENE);
    }

    /**
     * Method for creating a new army and write to a new .csv-file.
     * @return a newly created Army.
     */
    @FXML
    public Army createArmy() {
        CheckBox infantry = checkboxInfantryUnit;
        CheckBox ranged = checkboxRangedUnit;
        CheckBox cavalry = checkboxCavalryUnit;
        CheckBox commander = checkboxCommanderUnit;
        Text printSaveFileMessage = armySaveFileMessage;

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Army");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Army files", ".csv"));

        if (armyName.getText().isEmpty()) {
            PopupMessage.notificationErrorMessage("Error", "Army creation",
                    "Please give your army a name before saving it to a file.");
            throw new IllegalArgumentException("You need to give your army a name.");
        }

        Army army = new Army(armyName.getText());

        if (checkboxInfantryUnit.isSelected()) {
            List<Unit> units = new ArrayList<>();
            int health = Integer.parseInt(InfantryUnitHealth.getText());
            if (health > 1000 || health <= 0) {
                PopupMessage.notificationErrorMessage("Error", "Health",
                        "Health cannot be 0 or negative numbers and the maximum health value is 1000.");
            }
            int amount = Integer.parseInt(InfantryUnitAmount.getText());
            if (amount > 1000 || amount <= 0) {
                PopupMessage.notificationErrorMessage("Error", "Amount of units",
                        "The maximum amount of units is set to 1000, yet cannot be 0 or negative values.");
            }
            units = UnitFactory.getUnits("InfantryUnit", InfantryUnitName.getText(),
                    health, amount);
            army.addAllUnit(units);
        }

        if (checkboxRangedUnit.isSelected()) {
            List<Unit> units = new ArrayList<>();
            int health = Integer.parseInt(RangedUnitHealth.getText());
            if (health > 1000 || health <= 0) {
                PopupMessage.notificationErrorMessage("Error", "Health",
                        "Health cannot be 0 or negative numbers and the maximum health value is 1000.");
            }
            int amount = Integer.parseInt(RangedUnitAmount.getText());
            if (amount > 1000 || amount <= 0) {
                PopupMessage.notificationErrorMessage("Error", "Amount of units",
                        "The maximum amount of units is set to 1000, yet cannot be 0 or negative values.");
            }
            units = UnitFactory.getUnits("RangedUnit", RangedUnitName.getText(),
                    health, amount);
            army.addAllUnit(units);
        }

        if (checkboxCavalryUnit.isSelected()) {
            List<Unit> units = new ArrayList<>();
            int health = Integer.parseInt(CavalryUnitHealth.getText());
            if (health > 1000 || health <= 0) {
                PopupMessage.notificationErrorMessage("Error", "Health",
                        "Health cannot be 0 or negative numbers and the maximum health value is 1000.");
            }
            int amount = Integer.parseInt(CavalryUnitAmount.getText());
            if (amount > 1000 || amount <= 0) {
                PopupMessage.notificationErrorMessage("Error", "Amount of units",
                        "The maximum amount of units is set to 1000, yet cannot be 0 or negative values.");
            }
            units = UnitFactory.getUnits("CavalryUnit", CavalryUnitName.getText(),
                    health, amount);
            army.addAllUnit(units);
        }

        if (checkboxCommanderUnit.isSelected()) {
            List<Unit> units = new ArrayList<>();
            int health = Integer.parseInt(CommanderUnitHealth.getText());
            if (health > 1000 || health <= 0) {
                PopupMessage.notificationErrorMessage("Error", "Health",
                        "Health cannot be 0 or negative numbers and the maximum health value is 1000.");
            }
            int amount = Integer.parseInt(CommanderUnitAmount.getText());
            if (amount > 1000 || amount <= 0) {
                PopupMessage.notificationErrorMessage("Error", "Amount of units",
                        "The maximum amount of units is set to 1000, yet cannot be 0 or negative values.");
            }
            units = UnitFactory.getUnits("CommanderUnit", CommanderUnitName.getText(),
                    health, amount);
            army.addAllUnit(units);
        }

        if (!(infantry.isSelected() && ranged.isSelected() && cavalry.isSelected() && commander.isSelected())) {
             PopupMessage.notificationErrorMessage("Notification", "Army creation",
                     "It appears you have not ticked all the checkboxes for the respective units. " +
                             "For this version of Wargames, it is recommended that all four unit-checkboxes have " +
                             "been ticked before saving your army.");
        } else {
             File createdFile = fileChooser.showSaveDialog(view.getStage());
             File pathToDirectory = new File(
                     "src/main/resources/edu/ntnu/idatt2001/stenil/wargames/premade-armies");
             //Directory path from content root
             fileChooser.setInitialDirectory(pathToDirectory);
             try {
                 if (createdFile != null) {
                     ArmyFileWriter writer = new ArmyFileWriter();
                     writer.writeArmy(createdFile, army);
                     printSaveFileMessage.setText("Army saved! " +
                             "You can now proceed to the battle screen or create another army");
                 }
             } catch (IOException e) {
                 System.out.println("Something went wrong when creating your army: " + e);
             }
        }
        return null;
    }

    /**
     * Method for resetting the input TextFields in the create army menu.
     */
    public void resetInputFieldsForCreatingNewArmy() {
        armyName.setText("");
        armySaveFileMessage.setText("");

        checkboxInfantryUnit.setSelected(false);
        checkboxRangedUnit.setSelected(false);
        checkboxCavalryUnit.setSelected(false);
        checkboxCommanderUnit.setSelected(false);

        InfantryUnitName.setText("");
        RangedUnitName.setText("");
        CavalryUnitName.setText("");
        CommanderUnitName.setText("");

        InfantryUnitHealth.setText("");
        RangedUnitHealth.setText("");
        CavalryUnitHealth.setText("");
        CommanderUnitHealth.setText("");

        InfantryUnitAmount.setText("");
        RangedUnitAmount.setText("");
        CavalryUnitAmount.setText("");
        CommanderUnitAmount.setText("");
    }

    /**
     * Method for displaying error/info messages when trying to edit the attack and/or armor values for an Army.
     * Not yet implemented.
     */
    public void printErrorMessageWhenSelectingAttackOrArmor() {
        if (InfantryAttack.isHover() || RangedAttack.isHover() || CavalryAttack.isHover() || CommanderAttack.isHover()) {
            PopupMessage.notificationErrorMessage("Error", "Attack values",
                    "Unfortunately, you cannot edit the attack values in this version of Wargames.");
        }
        if (InfantryArmor.isHover() || RangedArmor.isHover() || CavalryArmor.isHover() || CommanderArmor.isHover()) {
            PopupMessage.notificationErrorMessage("Error", "Armor values",
                    "Unfortunately, you cannot edit the armor values in this version of Wargames.");
        }
    }
}

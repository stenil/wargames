package edu.ntnu.idatt2001.stenil.wargames;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * A Singleton class holding application data. In order to get an instance of an object,
 * use <code>getInstance()</code>.
 */
public class View {

    private static View instance;
    private Stage stage;
    private Scene currentScene;

    public final static String OPENING_SCENE = "views/wargames-opening-scene.fxml";
    public final static String OPENING_MENU = "views/wargames-opening-menu.fxml";
    public final static String ARMY_CREATION_MENU = "views/new-army-creation-menu.fxml";
    public final static String BATTLE_SCENE = "views/battle-scene.fxml";

    /**
     * An empty constructor for View.
     */
    private View() {
    }

    /**
     * Method for getting a single instance of the View class, which contains data.
     * @return the instance of the View class.
     */
    public static synchronized View getInstance() {
        if (instance == null) {
            instance = new View();
        }
        return instance;
    }

    /**
     * Method for retrieving the stage. The stage is a window-frame and considered "parent" of the View.
     * @return the stage.
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * Method for setting the stage. The stage is a window-frame and considered "parent" of the View.
     * @param stage is the stage which is to be set.
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * A method to set the current scene. In order to set the correct scene it is recommended to apply the static
     * constants in this class.
     * @param path is the pathname to an .fxml-file.
     * @throws IllegalArgumentException if the pathname or .fxml-file is incorrect.
     */
    public void setCurrentScene(String path) {
        FXMLLoader fxmlLoader = new FXMLLoader(View.class.getResource(path));
        try {
            this.currentScene = new Scene(fxmlLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("An error occurred when loading the .fxml file: " + e.getMessage());
        }
        updateStage();
    }

    /**
     * Method for updating the stage with the current scene.
     */
    private void updateStage() {
        this.stage.setScene(this.currentScene);
    }
}
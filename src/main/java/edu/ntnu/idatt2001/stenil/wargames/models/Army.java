package edu.ntnu.idatt2001.stenil.wargames.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * Class for holding an Army.
 */
public class Army {

    private String nameOfArmy;
    private List<Unit> units;

    /**
     * Simplified constructor for creating a new army
     * @param nameOfArmy a String with the name of the army
     */
    public Army(String nameOfArmy) {
        this.nameOfArmy = nameOfArmy;
        this.units = new ArrayList<>(); //"units" is the list
    }

    /**
     * Constructor for creation a new army
     * @param nameOfArmy a String with the name of the army
     * @param unit is a unit from the edu.ntnu.idatt2001.stenil.wargames.models.Unit list
     */
    public Army(String nameOfArmy, List<Unit> unit) {
        this.nameOfArmy = nameOfArmy;
        this.units = unit;
    }

    /**
     * Method for retrieving the army name
     * @return a String with the name of the army
     */
    public String getNameOfArmy() {
        return nameOfArmy;
    }

    /**
     * Method for retrieving a unit from the units ArrayList
     * @return a unit from the edu.ntnu.idatt2001.stenil.wargames.models.Unit ArrayList
     */
    public List<Unit> getUnit() {
        return units;
    }

    /**
     * Method for adding units to the units ArrayList
     * @param unit is a unit, derived from the edu.ntnu.idatt2001.stenil.wargames.models.Unit Superclass
     * @return a bool, true if adding a unit is successful
     */
    public boolean addUnit(Unit unit) {
        return this.units.add(unit);
    }

    /**
     * Method for adding multiple units to the units ArrayList
     * @param units is a unit(s), derived from the edu.ntnu.idatt2001.stenil.wargames.models.Unit Superclass
     */
    public void addAllUnit(List<Unit> units) {
        for (Unit unit: units) {
            this.units.add(unit);
        }
    }

    /**
     * Method for reming units from the units ArrayList
     * @param unit is a unit(s), derived from the edu.ntnu.idatt2001.stenil.wargames.models.Unit Superclass
     * @return a bool, true of removing a unit is successful
     */
    public boolean removeUnit(Unit unit) {
        return units.remove(unit);
    }

    /**
     * Method for checking if an army has units in it
     * @return a bool, returns true if units are in the army
     */
    public boolean hasUnits() {
        return !units.isEmpty(); //Returns false if list is empty (HAS! units)
    }

    /**
     * Method for retrieving all the units
     * @return all the units of the units ArrayList
     */
    public List<Unit> getAllUnits() {
        return units;
    }

    /**
     * Method for retrieving a random unit from an army
     * @return a random unit from an army
     */
    public Unit getRandomUnit() {
        int i = units.size();
        Random randomizer = new Random();
        return (units.get(randomizer.nextInt(i)));
    }

    /**
     * Method for retrieving all infantry units from an Army list.
     * @return The unit type 'Infantry Unit' from an Army as a List.
     */
    public List<Unit> getInfantryUnits () {
        return units.stream().filter(unit -> unit.getClass() == InfantryUnit.class).toList();
    }

    /**
     * Method for retrieving all ranged units from an Army list.
     * @return the unit type 'Ranged Unit' from an Army as a list.
     */
    public List<Unit> getRangedUnits () {
        return units.stream().filter(unit -> unit.getClass() == RangedUnit.class).toList();
    }

    /**
     * Method for retrieving all cavalry units from an Army list.
     * @return the unit type 'Cavalry Unit' from an Army as a list.
     */
    public List<Unit> getCavalryUnits () {
        return units.stream().filter(unit -> unit.getClass() == CavalryUnit.class).toList();
    }

    /**
     * Method for retrieving all commander units from an Army list.
     * @return the unit type 'Commander Unit' from an Army as a list.
     */
    public List<Unit> getCommanderUnits () {
        return units.stream().filter(unit -> unit.getClass() == CommanderUnit.class).toList();
    }

    @Override //
    public String toString() {
        return  nameOfArmy + " | " +
                "number of units " + units;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(nameOfArmy, army.nameOfArmy) && Objects.equals(units, army.units);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameOfArmy, units);
    }
}
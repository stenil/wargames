package edu.ntnu.idatt2001.stenil.wargames.controllers;

import edu.ntnu.idatt2001.stenil.wargames.View;

/**
 * Controller for wargames-opening-menu
 */
public class WargamesOpeningMenuController extends BaseController {

    public void navigateToBattleScene() {
        view.setCurrentScene(View.BATTLE_SCENE);
    }

    public void navigateToCreateNewArmyMenu() {
        view.setCurrentScene(View.ARMY_CREATION_MENU);
    }

    public void returnToHomeScreen() {
        view.setCurrentScene(View.OPENING_SCENE);
    }
}
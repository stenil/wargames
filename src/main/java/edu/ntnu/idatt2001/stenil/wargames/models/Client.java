package edu.ntnu.idatt2001.stenil.wargames.models;

public class Client {

    /**
     * The main method which runs the battle simulation application
     * @param args args
     */
    public static void main(String[] args) {

        Army armyOne = new Army("Human Army");
        Army armyTwo = new Army("Orc Army");

        for (int i = 0; i < 5; i++) {
            armyOne.addUnit(new RangedUnit("Archer " + i, 100));
        }

        for (int i = 0; i < 5; i++) {
            armyTwo.addUnit(new RangedUnit("Spearman " + i, 100));
        }

        Battle battle = new Battle(armyOne, armyTwo, Terrain.FORESTS);

        Army winner = battle.simulateBattle();

        System.out.println("The winner is " + winner + "\n");
    }
}
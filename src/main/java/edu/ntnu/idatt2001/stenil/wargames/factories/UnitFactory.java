package edu.ntnu.idatt2001.stenil.wargames.factories;

import edu.ntnu.idatt2001.stenil.wargames.models.*;

import java.util.ArrayList;
import java.util.List;

public class UnitFactory {

    /**
     * Factory for making a given amount of units of respective type.
     * The units will be of identical type, and feature identical name and health parameters.
     * @param unitType is the unit type as a String
     * @param unitName is the unit name as a String
     * @param unitHealth is the unit health as an Integer
     * @param amount is the amount of units as an Integer
     * @return the set amount of units of a given unit type.
     */
    public static List<Unit> getUnits(String unitType, String unitName, int unitHealth, int amount) {
        if (unitType.isEmpty()) {
            throw new IllegalArgumentException("Invalid unit type.");
        }
        if (amount > 1000) {
            throw new IllegalArgumentException("Too many units, tha maximum amount is 1000.");
        }
        List<Unit> units = new ArrayList<>();
        for (int i = 0; i < amount; i++){
            Unit unit = createObjects(unitType); //Calling method to create objects
            unit.setHealth(unitHealth);
            unit.setName(unitName);
            units.add(unit);
        }
        return units;
    }

    /**
     * Method for creating (dummy) unit objects, as necessary in order to create objects of the unit types.
     * @param klasse is the corresponding class of a unit
     * @return the units in question
     */
    public static Unit createObjects (String klasse) {
        if (klasse.equalsIgnoreCase("InfantryUnit")) {
            return new InfantryUnit("unitName", 15);
            //Using simplified constructor for an infantry unit
        }
        if (klasse.equalsIgnoreCase("RangedUnit")) {
            return new RangedUnit("unitName", 15);
            //Using simplified constructor for a ranged unit
        }
        if (klasse.equalsIgnoreCase("CavalryUnit")) {
            return new CavalryUnit("unitName", 15);
            //Using simplified constructor for a cavalry unit
        }
        if (klasse.equalsIgnoreCase("CommanderUnit")) {
            return new CommanderUnit("unitName", 15);
            //Using simplified constructor for a commander unit
        }
        throw new IllegalArgumentException("Invalid: Could not find any units.");
    }
}

package edu.ntnu.idatt2001.stenil.wargames.models;

import java.util.Random;

/**
 * Class for holding a Battle and its data.
 */
public class Battle {

    private Army armyOne;
    private Army armyTwo;
    private Terrain terrain;

    /**
     * Original constructor for a war scenario battle, featuring two armies
     * @param armyOne is the first army
     * @param armyTwo is the second army
     */
    public Battle (Army armyOne, Army armyTwo, Terrain terrain) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrain;
    }

    /**
     * Random unit from the two armies fights with each other until the number of units within an army is empty.
     * The dead units get removed from the army object, and the army that is first to lose all its units have
     * last the battle.
     * @return a String with the winning army
     */
    public Army simulateBattle() {
        Random random = new Random();

        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            int i = random.nextInt(2); //0 to 2, except 2, which is exclusive
            if (i == 0) {
                Unit armyOneUnit = armyOne.getRandomUnit();
                Unit armyTwoUnit = armyTwo.getRandomUnit();
                while (armyOneUnit.getHealth() > 0) {
                    armyOneUnit.attack(armyTwoUnit, terrain);
                    if (armyTwoUnit.getHealth() == 0 && armyTwoUnit.isDead()) {
                        armyTwo.removeUnit(armyTwoUnit);
                        break;
                    }
                    armyTwoUnit.attack(armyOneUnit, terrain);
                }
            } else {
                Unit armyTwoUnit = armyTwo.getRandomUnit();
                Unit armyOneUnit = armyOne.getRandomUnit();
                while (armyTwoUnit.getHealth() > 0) {
                    armyTwoUnit.attack(armyOneUnit, terrain);
                    if (armyOneUnit.getHealth() == 0 && armyOneUnit.isDead()) {
                        armyOne.removeUnit(armyOneUnit);
                        break;
                    }
                    armyOneUnit.attack(armyTwoUnit, terrain);
                }
            }
        }
        if (armyOne.hasUnits()) {
            return armyOne;
        } else {
            return armyTwo;
        }
    }

    @Override
    public String toString() {
        return "Battle | " +
                "Human Army " + armyOne +
                "Orc Army " + armyTwo;
    }

}

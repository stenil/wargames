package edu.ntnu.idatt2001.stenil.wargames.models;

/**
 * The terrains can either be plains, hills or forests
 */
public enum Terrain {
    PLAINS,
    HILLS,
    FORESTS
}